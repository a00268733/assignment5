package com.aditya.temperature;

public class TemperatureException extends Exception {
	private static final long serialVersionUID = 1L;

	public TemperatureException(String message) {
		super(message);
	}
}
