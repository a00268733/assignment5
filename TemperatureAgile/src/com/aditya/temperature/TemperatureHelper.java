package com.aditya.temperature;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TemperatureHelper {

	private static double fahToCel(double fahren){  
		return (fahren - 32.0d) * 5.0d / 9.0d; 
	}

	private static double celToFah(double celcius){  
		return ((celcius * 9.0d / 5.0d) + 32.0d);
	}

	public static double celsiusToFahrenheit(double celsius) throws TemperatureException {
		if(celsius < -273.15)
			throw new TemperatureException("TemperatureTooLowException: Temperature entered in celsius cannot be below absolute zero");
		if(celsius > 100)
			throw new TemperatureException("TemperatureTooHighException: Temperature entered in celsius cannot be higher than 100");

		return new BigDecimal(Double.toString(celToFah(celsius))).setScale(2, RoundingMode.HALF_UP).doubleValue();
	}

	public static double fahrenheitToCelsius(double fahren) throws TemperatureException {
		if(fahren < -459.67)
			throw new TemperatureException("TemperatureTooLowException: Temperature entered in fahrenheit cannot be below absolute zero");
		if(fahren > 212)
			throw new TemperatureException("TemperatureTooHighException: Temperature entered in fahrenheit cannot be higher than 212");

		return new BigDecimal(Double.toString(fahToCel(fahren))).setScale(2, RoundingMode.HALF_UP).doubleValue();
	}
}
