package com.aditya.test;

import com.aditya.temperature.TemperatureException;
import com.aditya.temperature.TemperatureHelper;

import junit.framework.TestCase;

public class TemperatureTests extends TestCase {

	/**
	 * TestCaseID         : 1
	 * TestDescription    : convert -500 Celsius to Fahrenheit
	 * TestCaseInput      : celsius = -500
	 * TestExpectedOutput : TemperatureTooLowException: Temperature entered in celsius cannot be below absolute zero
	 */
	public void testCelToFah001() {
		try {
			TemperatureHelper.celsiusToFahrenheit(-500);
			fail();
		} catch (TemperatureException e) {
			assertEquals("TemperatureTooLowException: Temperature entered in celsius cannot be below absolute zero", e.getMessage());
		}
	}

	/**
	 * TestCaseID         : 2
	 * TestDescription    : convert -273.15 Celsius to Fahrenheit
	 * TestCaseInput      : celsius = -273.15
	 * TestExpectedOutput : -459.67
	 */
	public void testCelToFah002() {
		try {
			assertEquals(-459.67, TemperatureHelper.celsiusToFahrenheit(-273.15));
		} catch (TemperatureException e) {
			fail();
		}
	}

	/**
	 * TestCaseID         : 3
	 * TestDescription    : convert 0 Celsius to Fahrenheit
	 * TestCaseInput      : celsius = 0
	 * TestExpectedOutput : 32
	 */
	public void testCelToFah003() {
		try {
			assertEquals(32.0, TemperatureHelper.celsiusToFahrenheit(0));
		} catch (TemperatureException e) {
			fail();
		}
	}

	/**
	 * TestCaseID         : 4
	 * TestDescription    : convert 100 Celsius to Fahrenheit
	 * TestCaseInput      : celsius = 100
	 * TestExpectedOutput : 212
	 */
	public void testCelToFah004() {
		try {
			assertEquals(212.0, TemperatureHelper.celsiusToFahrenheit(100));
		} catch (TemperatureException e) {
			fail();
		}
	}

	/**
	 * TestCaseID         : 5
	 * TestDescription    : convert 200 Celsius to Fahrenheit
	 * TestCaseInput      : celsius = 200
	 * TestExpectedOutput : TemperatureTooHighException: Temperature entered in celsius cannot be higher than 100
	 */
	public void testCelToFah005() {
		try {
			TemperatureHelper.celsiusToFahrenheit(200);
			fail();
		} catch (TemperatureException e) {
			assertEquals("TemperatureTooHighException: Temperature entered in celsius cannot be higher than 100", e.getMessage());
		}
	}

	/**
	 * TestCaseID         : 6
	 * TestDescription    : convert asd312 Celsius to Fahrenheit
	 * TestCaseInput      : celsius = asd312
	 * TestExpectedOutput : Wrong Input
	 */
	public void testCelToFah006() {
		try {
			TemperatureHelper.celsiusToFahrenheit(Double.parseDouble("asd312"));
			fail();
		} catch (Exception e) {
			assertEquals(true, true);
		}
	}
	
	/**
	 * TestCaseID         : 7
	 * TestDescription    : convert -500 Fahrenheit to Celsius
	 * TestCaseInput      : fahren = -500
	 * TestExpectedOutput : TemperatureTooLowException: Temperature entered in fahrenheit cannot be below absolute zero
	 */
	public void testFahToCel001() {
		try {
			TemperatureHelper.fahrenheitToCelsius(-500);
			fail();
		} catch (TemperatureException e) {
			assertEquals("TemperatureTooLowException: Temperature entered in fahrenheit cannot be below absolute zero", e.getMessage());
		}
	}

	/**
	 * TestCaseID         : 8
	 * TestDescription    : convert -459.67 Fahrenheit to Celsius
	 * TestCaseInput      : fahren = -459.67
	 * TestExpectedOutput : -273.15
	 */
	public void testFahToCel002() {
		try {
			assertEquals(-273.15, TemperatureHelper.fahrenheitToCelsius(-459.67));
		} catch (TemperatureException e) {
			fail();
		}
	}

	/**
	 * TestCaseID         : 9
	 * TestDescription    : convert 100 Fahrenheit to Celsius
	 * TestCaseInput      : fahren = 100
	 * TestExpectedOutput : 37.78
	 */
	public void testFahToCel003() {
		try {
			assertEquals(37.78, TemperatureHelper.fahrenheitToCelsius(100));
		} catch (TemperatureException e) {
			fail();
		}
	}

	/**
	 * TestCaseID         : 10
	 * TestDescription    : convert 212 Fahrenheit to Celsius
	 * TestCaseInput      : fahren = 212
	 * TestExpectedOutput : 100
	 */
	public void testFahToCel004() {
		try {
			assertEquals(100.0, TemperatureHelper.fahrenheitToCelsius(212));
		} catch (TemperatureException e) {
			fail();
		}
	}

	/**
	 * TestCaseID         : 11
	 * TestDescription    : convert 300 Fahrenheit to Celsius
	 * TestCaseInput      : fahren = 300
	 * TestExpectedOutput : TemperatureTooHighException: Temperature entered in fahrenheit cannot be higher than 212
	 */
	public void testFahToCel005() {
		try {
			TemperatureHelper.fahrenheitToCelsius(300);
			fail();
		} catch (TemperatureException e) {
			assertEquals("TemperatureTooHighException: Temperature entered in fahrenheit cannot be higher than 212", e.getMessage());
		}
	}

	/**
	 * TestCaseID         : 12
	 * TestDescription    : convert asd312 Fahrenheit to Celsius
	 * TestCaseInput      : fahren = asd312
	 * TestExpectedOutput : Wrong Input
	 */
	public void testFahToCel006() {
		try {
			TemperatureHelper.fahrenheitToCelsius(Double.parseDouble("asd312"));
			fail();
		} catch (Exception e) {
			assertEquals(true, true);
		}
	}
}
